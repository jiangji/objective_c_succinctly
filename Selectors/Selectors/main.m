//
//  main.m
//  Selectors
//
//  Created by Ryan Hodson on 11/8/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

NSString *askUserForAction() {
    // In the real world, this would be capture some
    // user input to determine which method to call
    NSString *theMethod = @"sayGoodbye";
    return theMethod;
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // Create a person and determine an action to perform
        Person *joe = [[Person alloc] init];
        joe.name = @"Joe";
        Person *bill = [[Person alloc] init];
        bill.name = @"Bill";
        joe.friend = bill;
        joe.action = NSSelectorFromString(askUserForAction());
        
        // Wait for an event...
        
        // Perform the action
        [joe coerceFriend];
        
    }
    return 0;
}
