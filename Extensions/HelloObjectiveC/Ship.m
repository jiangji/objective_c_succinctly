//
//  Ship.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Ship.h"


// The class extension
@interface Ship()

@property (strong, readwrite) Person *captain;

@end


// The standard implementation
@implementation Ship

@synthesize captain = _captain;

- (id)initWithCaptain:(Person *)captain {
    self = [super init];
    if (self) {
        // This WILL work because of the extension
        [self setCaptain:captain];
    }
    return self;
}

@end
