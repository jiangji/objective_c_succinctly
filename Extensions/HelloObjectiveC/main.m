//
//  main.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Ship.h"

int main(int argc, const char * argv[])
{
    // with ARC no special memory management code is needed
    // the compiler figures out what we had to add manually
    // earlier
    @autoreleasepool {
    
        Person *heywood = [[Person alloc] init];
        heywood.name = @"Heywood";
        Ship *discoveryOne = [[Ship alloc] initWithCaptain:heywood];
        NSLog(@"%@", [discoveryOne captain].name);
        
        Person *dave = [[Person alloc] init];
        dave.name = @"Dave";
        // This will NOT work because the property is still readonly
        // the property is read/write only when in the scope of the
        // extension
        // comment out to check
        // [discoveryOne setCaptain:dave];
    }
    
   
    return 0;
}

