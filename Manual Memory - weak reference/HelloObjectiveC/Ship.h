//
//  Ship.h
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface Ship : NSObject

- (Person *)captain;
- (void)setCaptain:(Person *)theCaptain;

@end