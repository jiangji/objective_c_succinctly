//
//  Person.m
//  ManualProperty
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Person.h"

@implementation Person
- (unsigned int)age {
    return _age;
}

- (void)setAge:(unsigned int)age {
    _age = age;
}
@end
