//
//  Person.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Person.h"

@implementation Person
-(void)sayHello{
    NSLog(@"Hello, my name is HAL.");
}
- (void)sayHelloToName:(NSString *)aName {
    NSLog(@"Hello %@, my name is HAL.", aName);
}
@end
