//
//  main.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        Person* somePerson = [[Person alloc] init];
        [somePerson sayHello];
        [somePerson sayHelloToName:@"Bill"];
        
    }
    return 0;
}

