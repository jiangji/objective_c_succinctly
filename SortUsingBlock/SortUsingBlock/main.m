//
//  main.m
//  SortUsingBlock
//
//  Created by Ryan Hodson on 11/8/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSMutableArray *numbers = [NSMutableArray arrayWithObjects:
                                   [NSNumber numberWithFloat:3.0f],
                                   [NSNumber numberWithFloat:5.5f],
                                   [NSNumber numberWithFloat:1.0f],
                                   [NSNumber numberWithFloat:12.2f], nil];
        
        [numbers
         sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
             float number1 = [obj1 floatValue];
             float number2 = [obj2 floatValue];
             if (number1 < number2) {
                 return NSOrderedAscending;
             } else if (number1 > number2) {
                 return NSOrderedDescending;
             } else {
                 return NSOrderedSame;
             }
         }];
        
        for (int i=0; i<[numbers count]; i++) {
            NSLog(@"%i: %0.1f", i, [[numbers objectAtIndex:i] floatValue]);
        }
        
    }
    return 0;
}

