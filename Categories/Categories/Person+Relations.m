//
//  Person+Relations.m
//  Categories
//
//  Created by Ryan Hodson on 11/8/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Person+Relations.h"

@implementation Person (Relations)


- (void)addFriend:(Person *)aFriend {
    [[self friends] addObject:aFriend];
}

- (void)removeFriend:(Person *)aFriend {
    [[self friends] removeObject:aFriend];
}

- (void)sayHelloToFriends {
    for (Person *friend in [self friends]) {
        NSLog(@"Hello there, %@!", [friend name]);
    }
}
@end
