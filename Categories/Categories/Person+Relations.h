//
//  Person+Relations.h
//  Categories
//
//  Created by Ryan Hodson on 11/8/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Person.h"

@interface Person (Relations)
- (void)addFriend:(Person *)aFriend;
- (void)removeFriend:(Person *)aFriend;
- (void)sayHelloToFriends;
@end
